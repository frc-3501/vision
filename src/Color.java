import java.awt.Dimension;
import java.awt.image.BufferedImage;

import boofcv.core.image.ConvertBufferedImage;
import boofcv.gui.image.ImagePanel;
import boofcv.gui.image.ShowImages;
import boofcv.io.webcamcapture.UtilWebcamCapture;
import boofcv.struct.image.ImageUInt8;

import com.github.sarxos.webcam.Webcam;

/**
 * @author Yuval Hermelin
 * @author Evan Yap
 */
public class Color {

	private static final float HUE = 0.9027565f;
	private static final float SATURATION = 0.8055556f;
	private static final float VALUE = 180.0f;
	private static final int TIMES_TO_RUN_ERODE_AND_DILATE = 5;
	private static final int BLACK_COLOR = 1;
	private static final int WHITE_COLOR = 255;
	private static final int WIDTH = 640;
	private static final int HEIGHT = 480;

	public static void main(String args[]) {
		Webcam webcam = Webcam.getDefault();
		UtilWebcamCapture.adjustResolution(webcam, WIDTH, HEIGHT);
		webcam.open();
		ImagePanel gui = new ImagePanel();
		gui.setPreferredSize(new Dimension(webcam.getImage().getWidth(), webcam
				.getImage().getHeight()));
		ShowImages.showWindow(gui, "Outlines_Vision");

		while (true) {
			BufferedImage image = webcam.getImage();
			BufferedImage afterChangeType = ImageUtil.changeToTypeIntRGB(image);
			ImageUInt8 binary;
			// Display pre-selected colors
			binary = ColorUtil
					.getColor(afterChangeType, HUE, SATURATION, VALUE);
			// binary = getGrayTotes(afterChangeType);
			BufferedImage finalImage = new BufferedImage(binary.width,
					binary.height, BufferedImage.TYPE_INT_RGB);
			ConvertBufferedImage.convertTo(binary, finalImage);
			ImageUtil.fixImage(binary, TIMES_TO_RUN_ERODE_AND_DILATE);
			ImageUtil.whitenImage(binary, BLACK_COLOR, WHITE_COLOR);
			ConvertBufferedImage.convertTo(binary, finalImage);
			int[] centroid = ImageUtil.getCentroid(binary);
			ImageUtil.drawImage(finalImage, gui, centroid);
		}
	}

}