
import georegression.metric.UtilAngle;

import java.awt.image.BufferedImage;

import boofcv.alg.color.ColorHsv;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.ImageUInt8;
import boofcv.struct.image.MultiSpectral;

public class ColorUtil {
	public static ImageUInt8 getColor(BufferedImage image, float hue, float saturation, float value)
	{
		MultiSpectral<ImageFloat32> input = ConvertBufferedImage.convertFromMulti(image, null, true, ImageFloat32.class);
		MultiSpectral<ImageFloat32> hsv = new MultiSpectral<ImageFloat32>(ImageFloat32.class, input.width, input.height, 3);

		// Convert into HSV
		ColorHsv.rgbToHsv_F32(input, hsv);

		// Euclidean distance squared threshold for deciding which pixels are
		// members of the selected set low since Hue and Saturation
		// need to be specific unlike Grayscale using value.
		float maxDist2 = 0.16f;

		// Extract hue and saturation bands which are independent of intensity
		ImageFloat32 H = hsv.getBand(0);
		ImageFloat32 S = hsv.getBand(1);
		// ImageFloat32 V = hsv.getBand(2);
		// for color

		// Adjust the relative importance of Hue and Saturation.
		// Hue has a range of 0 to 2*PI and Saturation from 0 to 1.
		float adjustUnits = (float) (Math.PI / 2.0);

		// step through each pixel and mark how close it is to the selected
		// color
		BufferedImage output = new BufferedImage(input.width, input.height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < hsv.height; y++)
		{
			for (int x = 0; x < hsv.width; x++)
			{
				// Hue is an angle in radians, so simple subtraction doesn't
				// work
				float dh = UtilAngle.dist(H.unsafe_get(x, y), hue);
				float ds = (S.unsafe_get(x, y) - saturation) * adjustUnits;
				// float dv = (V.unsafe_get(x, y)-value)*adjustUnits; No need
				// for value.

				// this distance measure is a bit naive, but good enough for to
				// demonstrate the concept
				float dist2 = dh * dh + ds * ds;
				if (dist2 <= maxDist2)
				{
					output.setRGB(x, y, image.getRGB(x, y));
				}
			}
		}

		ImageUInt8 binary = ImageUtil.returnBinary(output);

		return binary;
	}
	

	public static ImageUInt8 getGrayTotes(BufferedImage image)
	{
		MultiSpectral<ImageFloat32> input = ConvertBufferedImage.convertFromMulti(image, null, true, ImageFloat32.class);
		MultiSpectral<ImageFloat32> hsv = new MultiSpectral<ImageFloat32>(ImageFloat32.class, input.width, input.height, 3);

		// Convert into HSV
		ColorHsv.rgbToHsv_F32(input, hsv);

		float value = 107.0f;

		// Euclidean distance squared threshold for deciding which pixels are
		// members of the selected set low since Hue and Saturation
		// need to be specific unlike Grayscale using value.
		float maxDist2 = 0.16f;

		// Extract hue and saturation bands which are independent of intensity

		ImageFloat32 V = hsv.getBand(2);
		// for color

		// Adjust the relative importance of Hue and Saturation.
		// Hue has a range of 0 to 2*PI and Saturation from 0 to 1.
		float adjustUnits = (float) (Math.PI / 2.0);

		// step through each pixel and mark how close it is to the selected
		// color
		BufferedImage output = new BufferedImage(input.width, input.height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < hsv.height; y++)
		{
			for (int x = 0; x < hsv.width; x++)
			{
				// Hue is an angle in radians, so simple subtraction doesn't
				// work

				float dv = (V.unsafe_get(x, y) - value) * adjustUnits;

				// this distance measure is a bit naive, but good enough for to
				// demonstrate the concept
				float dist2 = dv * dv;
				if (dist2 <= maxDist2)
				{
					output.setRGB(x, y, image.getRGB(x, y));
				}
			}
		}

		ImageUInt8 binary = ImageUtil.returnBinary(output);

		return binary;
	}
}