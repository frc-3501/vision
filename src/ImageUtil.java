import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import boofcv.alg.filter.binary.BinaryImageOps;
import boofcv.alg.filter.binary.ThresholdImageOps;
import boofcv.alg.misc.ImageStatistics;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.gui.image.ImagePanel;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.ImageUInt8;

public class ImageUtil {

	public static ImageUInt8 returnBinary(BufferedImage image) {
		// For now, until this method is fixed
		ImageFloat32 convertedImage = new ImageFloat32(image.getWidth(),
				image.getHeight());

		ConvertBufferedImage.convertFrom(image, convertedImage);

		ImageUInt8 binary = new ImageUInt8(image.getWidth(), image.getHeight());

		new BufferedImage(image.getWidth(), image.getHeight(),
				BufferedImage.TYPE_INT_RGB);

		// the mean pixel value is often a reasonable threshold when creating a
		// binary image

		double mean = ImageStatistics.mean(convertedImage);

		// create a binary image by thresholding

		ThresholdImageOps.threshold(convertedImage, binary, (float) mean, true);

		// reduce noise with some filtering

		ImageUInt8 filtered = BinaryImageOps.erode8(binary, 1, null);

		filtered = BinaryImageOps.dilate8(filtered, 1, null);

		return filtered;
	}

	public static BufferedImage changeToTypeIntRGB(BufferedImage image) {

		// Creates a new buffered image of type int rgb (the one we need for
		// everything to work
		BufferedImage output = new BufferedImage(image.getWidth(),
				image.getHeight(), BufferedImage.TYPE_INT_RGB);

		// Loops through all of original image and gives output the image
		// properties (except for the new type output has)
		for (int y = 0; y < image.getHeight(); y++)
			for (int x = 0; x < image.getWidth(); x++)
				output.setRGB(x, y, image.getRGB(x, y));

		return output;
	}

	public static ImageUInt8 fixImage(ImageUInt8 image,
			int TIMES_TO_RUN_ERODE_AND_DILATE) {
		// Dilate and erode to get rid of extra pixels
		image = BinaryImageOps.dilate8(image, TIMES_TO_RUN_ERODE_AND_DILATE,
				null);
		image = BinaryImageOps.erode8(image, TIMES_TO_RUN_ERODE_AND_DILATE,
				null);
		return image;
	}

	public static ImageUInt8 whitenImage(ImageUInt8 image, int BLACK_COLOR,
			int WHITE_COLOR) {
		// Set the value for the white pixels (That actually shows black) to
		// actually show white
		for (int x = 0; x < image.width; x++)
			for (int y = 0; y < image.height; y++)
				if (image.get(x, y) == BLACK_COLOR)
					image.set(x, y, WHITE_COLOR);
		return image;
	}

	public static void drawImage(BufferedImage finalImage, ImagePanel gui,
			int[] centroid) {
		Graphics2D g2d = finalImage.createGraphics();
		g2d.setStroke(new BasicStroke(5));
		g2d.setColor(java.awt.Color.cyan);
		g2d.drawOval(centroid[0], centroid[1], 10, 10);
		gui.repaint();
		gui.setBufferedImageSafe(finalImage);
	}

	public static int[] getCentroid(ImageUInt8 input) {
		int blackpixels = 0;
		int x = 0;
		int y = 0;
		for (int i = 0; i < input.getWidth(); i++) {
			for (int j = 0; j < input.getHeight(); j++) {
				if (input.get(i, j) == 0) {
					blackpixels++;
					x += i;
					y += j;
				}
			}
		}
		if (blackpixels != 0) {
			x /= blackpixels;
			y /= blackpixels;
		}
		return new int[] { x, y, blackpixels };

	}

}