
import georegression.struct.line.LineSegment2D_F32;
import georegression.struct.point.Point2D_F32;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import boofcv.abst.feature.detect.line.DetectLineSegmentsGridRansac;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.factory.feature.detect.line.FactoryDetectLineAlgs;
import boofcv.struct.image.ImageFloat32;
import boofcv.struct.image.ImageSingleBand;

public class LinesUtil {
	private static ArrayList<LineSegment2D_F32> segments;
	
	public static ArrayList<Point2D_F32> findCorners(BufferedImage image, int WIDTH, int HEIGHT)
	{
		@SuppressWarnings("rawtypes")
		ImageSingleBand input = ConvertBufferedImage.convertFromSingle(image, null, ImageFloat32.class);
		// Comment/uncomment to try a different type of line detector
		DetectLineSegmentsGridRansac<ImageFloat32, ImageFloat32> detector = FactoryDetectLineAlgs.lineRansac(40, 30, 2.36, true, ImageFloat32.class, ImageFloat32.class);
		ArrayList<LineSegment2D_F32> found = (ArrayList<LineSegment2D_F32>) detector.detect((ImageFloat32) input);
		segments = found;
		ArrayList<Point2D_F32> corners = cornerFinder(found, WIDTH, HEIGHT);
		return corners;
	}

	private static ArrayList<Point2D_F32> cornerFinder(ArrayList<LineSegment2D_F32> lines, int WIDTH, int HEIGHT)
	{
		// Initilize the four corners
		Point2D_F32 topLeft = new Point2D_F32(0, 0);
		Point2D_F32 topRight = new Point2D_F32(WIDTH, 0);
		Point2D_F32 bottomLeft = new Point2D_F32(0, HEIGHT);
		Point2D_F32 bottomRight = new Point2D_F32(WIDTH, HEIGHT);

		int i = 0;
		for (LineSegment2D_F32 lineSegment : lines)
		{
			//System.out.println("LineSegment: " + i + ". lineSegment.a.x: " + lineSegment.a.x + ". lineSegment.a.y: " + lineSegment.a.y + ". lineSegment.b.x: " + lineSegment.b.x + ". lineSegment.b.y: " + lineSegment.b.y);

			// Check to find four corners
			if (lineSegment.a.x > topLeft.x && lineSegment.a.y > topLeft.y)
				topLeft = lineSegment.a;
			if (lineSegment.b.x > topLeft.x && lineSegment.b.y > topLeft.y)
				topLeft = lineSegment.b;

			if (lineSegment.a.x < topRight.x && lineSegment.a.y > topRight.y)
				topRight = lineSegment.a;
			if (lineSegment.b.x < topRight.x && lineSegment.b.y > topRight.y)
				topRight = lineSegment.b;

			if (lineSegment.a.x > bottomLeft.x && lineSegment.a.y < bottomLeft.y)
				bottomLeft = lineSegment.a;
			if (lineSegment.b.x > bottomLeft.x && lineSegment.b.y < bottomLeft.y)
				bottomLeft = lineSegment.b;

			if (lineSegment.a.x < bottomRight.x && lineSegment.a.y < bottomRight.y)
				bottomRight = lineSegment.a;
			if (lineSegment.b.x < bottomRight.x && lineSegment.b.y < bottomRight.y)
				bottomRight = lineSegment.b;
			i++;
		}
		
		ArrayList<Point2D_F32> corners = new ArrayList<Point2D_F32>();
		corners.add(topLeft);
		corners.add(topRight);
		corners.add(bottomRight);
		corners.add(bottomLeft);
		return corners;
	}
	
	public static ArrayList<LineSegment2D_F32> getSegments()
	{
		return segments;
	}
}